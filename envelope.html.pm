#lang pollen
◊define-meta[title]{Envelope}

The envelope is a special property of a note. Similar to how a pitch curve defines pitch over time, envelope defines intensity over time.