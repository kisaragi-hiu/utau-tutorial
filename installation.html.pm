#lang pollen

◊title{UTAU 安裝}

UTAU 的安裝需要先解決編碼的問題。因為 UTAU 使用日文語系，若直接用中文語系安裝，預設音源會出現亂碼。

◊section{Windows}

有兩種解決語系問題的方法：切換到日文語系，或使用 Locale Emulator。

◊steps{
◊step{下載 UTAU。到 ◊link["http://utau2008.web.fc2.com"]{UTAU 官網} →「ダウンロード」→ 選 v4.18e。
◊image["install-download.png"]}
◊step{安裝 Locale Emulator。
下載 → 解壓縮 → 移動到固定位置 (這裡用 Program Files) → 執行 LEInstaller
◊image["install-locale-emulator-1.png"]
}
◊step{「為目前使用者安裝」(Install for current user) → 右鍵 UTAU 安裝檔 → Locale Emulator → Run in Japanese
◊image["install-locale-emulator-2.png"]
}
◊step{跟著安裝檔指示。}
}

◊section{Linux}
Install wine first.

Download UTAU from its website.

Create a new wineprefix by running:
◊highlight['sh]{
env WINEPREFIX=~/.utau winecfg}

This command makes wine create a new wineprefix at ◊path{~/.utau}, then open the wine configuration utility in it. Close the window.

◊sidenote{To use a wineprefix when running wine, set the environment variable $WINEPREFIX to the path of it. Now wine will run in the context of that folder. To start over, simply delete the folder.}

Make sure the Japanese locale is enabled. On Ubuntu, that means installing the Japanese language pack from settings → languages. On Arch, that means to uncomment "ja_JP.utf8" in ◊path{/etc/locale.gen}, then running ◊code{sudo locale-gen}.

Now install UTAU by running:
◊highlight['sh]{
env LANG=ja_JP.utf8 WINEPREFIX=~/.utau wine /path/to/utau/installer.exe}

Setting $LANG to ja_JP.utf8 should resolve encoding issues.

◊sidenote{If the latest version doesn't install, try 0.4.16.}
