#lang pollen
◊define-meta[title]{Notes}

A note is the basis of an UTAU vocal.

To make UTAU sing, each note is rendered by tool2 with its properties, then tool1 combines them together.

Notes have Note Properties.