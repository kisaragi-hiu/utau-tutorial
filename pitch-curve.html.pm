#lang pollen
◊define-meta[title]{Pitch curves}

A pitch curve is a special note property. As the name suggests, it is a curve defining the pitch of the note over time.

There are two different pitch curve models in UTAU, "mode1" and "mode2".

◊later-image{Mode1}

◊later-image{Mode2}