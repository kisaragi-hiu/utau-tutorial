#lang pollen
◊define-meta[title]{Flags}

Flags are extra per-note options for the renderer (specifically, tool2).

Per-note flags can be edited in ◊field{Flags} in note properties. Project-wide flags can be edited at Project Settings, in ◊field{Renderer Options}.

Flags are written as [name][value][name][value]…. For example, in
◊utauflags{
g3A20O23
}

"g" is set to 3, "A" to 20, and "O" to 23.

Each renderer has its own set of flags, although some flags are implemented in many renderers.
