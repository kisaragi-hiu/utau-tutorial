#lang racket
(require threading
         pollen/template
         pollen/decode
         pollen/tag
         pollen/unstable/pygments
         txexpr
         "./link.rkt")

(provide (all-defined-out)
         highlight
         (all-from-out "./link.rkt"))

; newline decoder
(provide root)
(define (root . elements)
   (txexpr 'root empty (decode-elements elements
                        #:txexpr-elements-proc decode-paragraphs)))

; markup
(define (R text ruby) `(ruby ,text (rt ,ruby)))

; from pollen-tfl
; http://docs.racket-lang.org/pollen-tfl/_pollen_rkt_.html?q=link#%28elem._%28chunk._~3cimage~3e~3a1%29%29
(define (image src #:width [width "100%"] #:border [border? #t])
  (define img-tag (attr-set* '(img) 'style (format "width: ~a" width)
                             'src (build-path "images" src)))
  (if border?
      (attr-set img-tag 'class "bordered")
      img-tag))

(define (site-ref post . text)
  `(a ([href ,(string-append "/blog/" post)])
      ,@text))

(define key (default-tag-function 'code))

(define (title . text)
  `(h1 ([class "title"]) ,@text))

(define (section . text)
  `(h2 ([class "section"] [id ,(first text)]) ,@text))

(define (steps . elements)
  `(ol ([class "steps"]) ,@elements))

(define (step . elements)
  `(li ([class "step"]) ,@elements))

(define (items . elements)
  `(ul ([class "items"]) ,@elements))

(define (item . elements)
  `(li ([class "item"]) ,@elements))

(define (utauflags . text)
  (~> (string-join text)
      (string-replace _ "\n" "")
      (string->list _) ; ListOf Char
      (map string _) ; ListOf String
      (map
       (λ (x) `(span ([class
                         ; Assign Pygments classes
                         ,(if (string->number x)
                              "mi" ; class for Integer
                              "k")]) ; class for Keyword
                      ,x)) _)
      (append '(span ([class "highlight"])) _)))
