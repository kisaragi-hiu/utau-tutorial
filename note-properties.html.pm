#lang pollen
◊define-meta[title]{Note Properties}

Lyric, Note, Length: self-explainatory.
These can be edited on the Piano Roll: Lyric by double-clicking on a note, "Note" (I call it pitch to avoid confution) by moving the note up and down, and Length by dragging the edges of the note.
If a note isn't changing size when you drag its edges, hold down ◊key{Ctrl} to shrink the adjacent note as you expand this, or ◊key{Shift} to resize while pushing everything further.
◊; Video works better here.

Intensity is the volume for the entire note, sort of. It can be visualized on the Piano Roll using the "~" button (2).

Consonant Velocity is how fast a note plays. Increasing the speed of a note makes the consonant sound shorter, hence the name. The consonant is two times longer at a value of 0 (note plays at 50%), and gets shorter the higher this number is. A value of 200 is half the length for the consonant.