◊; This is the script for my UTAU tutorial.
◊; Write it as a script.

◊section{Voice synthesis and UTAU}

UTAU is a voice synthesis platform.

Voice synthesis is a process of taking a set of samples, a note sheet, and producing a sound in the end. It is just like any other instruments.

Other voice synthesis platforms include VOCALOID and CeVIO.

UTAU is made up of voicebanks (= sample sets), project files called USTs (= note sheet), along with the main editor and render engines.

What makes UTAU unique is that all components are replacable. Surrounding the UST format, there are plenty of render engines, editing plugins, and most importantly, thousand upon thousands of voicebanks.

◊section{Setup}

UTAU has to be installed in the Japanese locale to avoid encoding issues. There are two ways to do this: switching Windows to use the Japanese locale, or launching UTAU using a locale emulator.

◊steps["Switch Windows locale"]{
    ◊step/gui{Control Panel → Time → Locale → Management → Non-unicode program language → Japanese (Japan)}
}

◊image["install-locale-emulator-1.png"]
◊image["install-locale-emulator-2.png"]

◊steps["Install Locale Emulator"]{
    ◊step{Download Locale Emulator from ◊link["http://pooi.moe/Locale-Emulator"]{its website}}
    ◊step{unzip and move to a permanent location (here I'm using Program Files)}
    ◊step{run LEInstaller}
    ◊step{Install for current user}
}

Now install UTAU itself.
◊image["install-download.png"]}
◊steps["Install UTAU"]{
    ◊step{Download UTAU from ◊link["http://utau2008.web.fc2.com"]{its official website}}
    ◊step{Either directly open, or right click → Locale Emulator → run in Japanese}
    ◊step{Go through the install process.}
}

Explain:
Voicebank

English.
Welcome to this UTAU tutorial. This is part 0, the Setup.

I'm going to assume you know Kana and some Japanese already, 'cause I'm not going to explain Japanese in this series. If you don't, most of this should still make sense; I'd say utau is actually a good way to learn Kana. Anyways.

◊introduce-utau
◊define[introduce-utau]{
UTAU is a voice synthesizer platform. It consists of an editor, render engines, voicebanks, and plugins, along with some voicebank creation tools that I won't get into in this series.
To produce a vocal with UTAU, the process is:
Edit the project file with the editor with some parameters, the editor will tell render engines what to do, the render engines will produce a voice from the instructions and the voicebank.
}

Download UTAU from ◊|utau-homepage|, then install it.
Note. If ◊|not-jp-locale|, use the Locale Emulator.
Extra. If you're using Linux, create a new wine prefix and run the installer.

Next, we're going to need some voicebanks.
Voicebanks are sound definition files of a voice of a character, also called an UTAUloid. I'm going to use Xia and Uzuki in my demo in this series; I don't know about English VBs.
◊xia-dl
◊uzuki-dl

To install a voicebank:
(if uar
    dnd-onto-utau
    extract-to-utau-dir)

Now start UTAU, and you should see this screen.
◊utau-first-start-screenshot

◊explain-ui-project-settings
◊define[explain-ui-project-settings]{
Project settings: click the region below thd utauloid's avatar. Here you can set the name of the project, what file should renders be saved to, project-wide flags, the voicebank to use, and which render engines to use.
}
◊explain-ui-note-basics
