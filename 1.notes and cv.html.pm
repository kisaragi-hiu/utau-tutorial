◊episode{1. Notes, basic tuning, CV, VCV}

note
Pitch
Flags
tool1
tool2
envelope
cv
vcv

Last video, I left off on note basics. This episode will be about how to manipulate notes to achieve the desired voice, ie. tuning.

◊explain-ui-note-display-mode
◊explain-ui-note-manipulation
◊define[explain-ui-note-manipulation]{
Drag a note's edge to change its size.
Hold ctrl and drag its edge to change its proportion with the other note.
Hold shift and drag its edge to change its size affecting all subsequent notes.
◊demo
}

◊explain-note-lyrics
◊explain-cv
◊explain-envelope
◊explain-vcv
◊explain-cvvc

◊explain-pitch-mode2
◊explain-tool1&tool2
◊explain-flags
