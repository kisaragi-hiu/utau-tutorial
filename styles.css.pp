#lang pollen

◊(define font/title "'Makinas'")
◊(define font/section "'Overpass'")
◊(define font/body "'Open Sans'")

◊(define color "#222")
◊(define multiplier 1.3)

@font-face {
  font-family: 'Makinas';
  src: local('Makinas'),
       url('fonts/makinas_scrap/Makinas-Scrap-5.otf');
}

body{
  font-size: 2.4rem;
  line-height: 1.5;
  font-family: ◊|font/body|, 'Noto Sans CJK TC Regular', sans-serif;
}

h1 {
  font-family: ◊|font/body|, 'Noto Sans CJK TC Light', sans-serif;
  font-size: 2.5em;
  line-height: 0.5;
}

h2 {
  font-family: ◊|font/body|, sans-serif;
  font-size: 1.8em;
  font-weight: 400;
  line-height: 0.1;
}

h3 {
  font-family: ◊|font/body|, sans-serif;
  font-size: 1.4em;
  font-weight: 700;
  line-height: 0.1;
}

pre {
  background-color: #eee;
  margin: 1em;
  overflow: auto;
  white-space: pre-wrap;
  word-wrap: break-word;
  color: #444;
}

a {
  color: #8944AF;
  background-color: #eee;
}

img {
  border: 1px solid #aaa;
}

.title {
  font-family: ◊|font/title|, sans-serif;
  margin-bottom: 10rem;
}

.section {
  font-family: ◊|font/section|, sans-serif;
  margin-top: 5rem;
}

.youtube {
  color: red;
}

.pixiv {
  color: #0096db;
}

.niconico {
  color: #42BB76;
}

