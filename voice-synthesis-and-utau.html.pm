#lang pollen

◊title{歌聲合成、UTAU}

歌聲合成是指將一個歌聲的譜轉換成歌聲的過程。歌聲合成的軟體會拿聲音的樣本，根據樂譜的定義，轉換成最終的歌聲。

比較大宗的歌聲合成軟體有 ◊glossary{VOCALOID}、◊glossary{UTAU}、◊glossary{CeVIO} 等等。

在 UTAU，「聲音的樣本」是◊glossary{音源}，而「樂譜」是打開 UTAU 時的專案檔案 ◊glossary{UST}。

◊section{UTAU}

UTAU 主要由數個部分構成：

◊items{
◊item{◊glossary{音源}}
◊item{◊glossary{UST} 專案檔案}
◊item{◊glossary{合成引擎}}
}

